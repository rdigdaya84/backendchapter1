import java.util.Arrays;

public class Topic4Soal3 {
    public static void main(String[] args) {

        char[] text = {'M', 'A', 'K', 'A', 'N', 'N', 'A', 'S', 'I'};

        Arrays.sort(text);

        System.out.println("Hasil Pengurutan Adalah ");
        for(int i=0;i< text.length;i++)
        {
            System.out.print(" "+ text[i]);
        }
    }
}
