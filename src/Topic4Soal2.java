import java.util.Arrays;

public class Topic4Soal2 {
    public static void main(String[] args){
        int[] data = { 6, 6, 5, 9, 2};

        for(int index=0; index<data.length; index++)
        {
            for(int index2=0; index2<data.length-index-1; index2++)
            {

                if(data[index2]<data[index2+1])
                {
                    int temp=data[index];
                    data[index2]=data[index2+1];
                    data[index2+1]=temp;
                }

            }
        }

        System.out.println("Hasil Pengurutan Adalah ");
        for(int i=0;i< data.length;i++)
        {
            System.out.print(" "+ data[i]);
        }

    }
}
