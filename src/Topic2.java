public class Topic2 {
    public static void main(String[] args){
        System.out.println("Hallo Binar");
        method1();

        String pertama = "Pertama";
        method2(pertama);
        method2("kedua");
        method2("ketiga");

        System.out.println(method3("tes return value"));
//        TODO: tes todo
        String cobaLagi = method4("coba kata Baru" , "a");
        System.out.println(cobaLagi);
    }

    public static void method1(){
        System.out.println("ini method1");
    }

    public static void method2(String parameter){
        System.out.println(parameter);
    }

    private static String method3(String params){
        return params;
    }

    /**
     *   Method yang digunakan untuk menghapus salah stu karakter dari text
     *   @param par Text yang ingin dihapus salah satu karakter
     *   @param CharHapus yang ingin dihapus
     *   @return  nilai yang sudah dihapus
     **/
    private static String method4(String par, String CharHapus){
        String hasil = par.replaceAll(CharHapus, "");
        return hasil;
    }
}
