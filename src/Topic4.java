public class Topic4 {

    public static void main(String[] args){

//        deklarasi array
        int array1[] = new int[5];
        int[] array2 = new int[5];
        // {0, 0, 0, 0, 0}

        // deklarasi dengan nilai
        int[] array3 = {1, 2, 3, 4, 5};
        System.out.println("Test index : " + array3[3]);

        //menggubah nilai array
        array3[3] = 99;
        System.out.println("Test index : " + array3[3]);

//        if -  else if  - else
        int nilai = 85;
        String nilaiHuruf = "";
        if(nilai >= 80){
            nilaiHuruf = "A";
        }else if(nilai >= 70 && nilai < 80){
            nilaiHuruf = "B";
        }else if(nilai >= 60 && nilai < 70){
            nilaiHuruf = "C";
        }else if(nilai >= 50 && nilai < 60){
            nilaiHuruf = "D";
        }else{
            nilaiHuruf = "E";
        }
        System.out.println(nilaiHuruf);

        // if ternary / Operator ternary
        int valA = 3, valB = 4;
        String hasil = valA > valB? "gedean a":"gedean b";
        System.out.println(hasil);



//        Swith case  = Satu nilai
        String grade = "B";
        switch(grade) {
            case "A" :
                System.out.println("grade A lur");
                break;
                case "B" :
                System.out.println("grade B lur");
                break;
                default:
                System.out.println("grade nol lur");
                break;
        }

        //for
        int[] data = {1, 2, 3, 4, 5};
        int[] sumData = new int[5];
        for(int index = 0; index < data.length; index++){
            int sum = data[index];
            for(int index2 = index+1; index2<data.length; index2++){
                sum += data[index2];
            }
            sumData[index] = sum;
        }
        for(int index = 0; index < sumData.length; index++) {
            System.out.print(sumData[index] + " ");
        }


        //2 for secara bersamaan
        for(int variable1=20, variable2=0;
            variable1>=10&&variable2<=5; variable1--,variable2++){
            System.out.println("\nNilai variable1= "+ variable1);
            System.out.println("Nilai variable2= "+variable2);
        }

        // for in for
        int rows = 5;
        for (int i=1; i<=rows; i++){
            for (int j = 1; j<=i; j++){
                System.out.print(j+" ");
            }
            System.out.println(" ");
        }


        System.out.println("\n" + recursion(3));
    }

    //rekursif
    public static int recursion(int k){
        if(k> 0){
            // k + (k-1) + (k-2) + (k-3) .... (k-n)
            return k + recursion(k-1);
        }else{
            return 0;
        }
    }
}
