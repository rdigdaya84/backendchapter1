import java.util.Scanner;

public class Challenge_1 {

    public static void main(String[] args){
      menuUtama();
    }

    public static void line(){
        for(int i=0; i<=40; i++){
            System.out.print("-");
        }
        System.out.println();
    }

    private static void menuUtama() {
        line();
        System.out.println("Kalkulator Penghitung Luas dan Volume");
        line();
        System.out.println("Menu");
        System.out.println("1. Hitung Luas Bidang ");
        System.out.println("2. Hitung Volume");
        System.out.println("0. Tutup Aplikasi");
        line();

//      input
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan Pilihanmu : ");
        int inputMenu = input.nextInt();
        switch(inputMenu){
            case 1:
                menuLuasBidang();
                break;
            case 2:
                menuVolume();
                break;
            default:
                System.out.println("Terimakasih telah menggunakan aplikasi kami :)");
                break ;
        }
    }

    private static void menuLuasBidang() {
        line();
        System.out.println("Pilih bidang yang akan dihitung");
        line();
        System.out.println("1. Persegi");
        System.out.println("2. Lingkaran");
        System.out.println("3. Segitiga");
        System.out.println("4. Persegi panjang");
        System.out.println("0. kembali ke menu sebelumnya");
        line();

//      input
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan Pilihanmu : ");
        int inputMenu = input.nextInt();
        switch(inputMenu){
            case 1:
                luasPersegi();
                break;
            case 2:
                luasLingkaran();
                break;
            case 3:
                luasSegitiga();
                break;
            case 4:
                luasPersegiPanjang();
                break;
            default:
                menuUtama();
            break ;
        }
    }

    //    menu volume
    private static void menuVolume() {
        line();
        System.out.println("Pilih bidang yang akan dihitung");
        line();
        System.out.println("1. Kubus");
        System.out.println("2. Balok");
        System.out.println("3. Tabung");
        System.out.println("0. kembali ke menu sebelumnya");
        line();

    //      input
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan Pilihanmu : ");
        int inputMenu = input.nextInt();
        switch(inputMenu){
            case 1:
                volumeKubus();
                break;
            case 2:
                volumeBalok();
                break;
            case 3:
                volumeTabung();
                break;
            default:
                menuUtama();
                break ;
        }
    }

    // ke menu utama
    public static void promptEnterKey(){
        System.out.print("tekan Enter untuk kembali ke menu utama ");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }


    //Luas persegi 1.1
    public static void luasPersegi(){
        line();
        System.out.println("Anda memilih Persegi");
        line();

        Scanner inputSisi = new Scanner(System.in);

        System.out.print("Masukkan Panjang Sisi: ");
        int sisi = inputSisi.nextInt();
        int luas = sisi * sisi;

        System.out.println("\n Processing... \n");
        System.out.println("Luas dari Persegi adalah " + luas);
        line();

        promptEnterKey();
        menuUtama();
    }
    //Luas Lingkaran 1.2
    public static void luasLingkaran(){
        line();
        System.out.println("Anda memilih Lingkaran");
        line();

        Scanner inputJariJari = new Scanner(System.in);

        System.out.print("Masukkan Panjang Jari-jari: ");
        int jariJari = inputJariJari.nextInt();
        double luas = 3.14 * jariJari * jariJari;

        System.out.println("\n Processing... \n");
        System.out.println("Luas dari Lingkaran adalah " + luas);
        line();

        promptEnterKey();
        menuUtama();
    }
    //Luas Segitiga 1.3
    public static void luasSegitiga(){
        line();
        System.out.println("Anda memilih Lingkaran");
        line();

        Scanner inputAlas = new Scanner(System.in);
        Scanner inputTinggi = new Scanner(System.in);

        System.out.print("Masukkan Alas: ");
        int alas = inputAlas.nextInt();
        System.out.print("Masukkan Tinggi: ");
        int tinggi = inputTinggi.nextInt();
        double luas = 0.5 * alas * tinggi;

        System.out.println("\n Processing... \n");
        System.out.println("Luas dari Segitiga adalah " + luas);
        line();

        promptEnterKey();
        menuUtama();
    }
    //Luas persegi panjang 1.4
    public static void luasPersegiPanjang(){
        line();
        System.out.println("Anda memilih Persegi Panjang");
        line();

        Scanner inputPanjang = new Scanner(System.in);
        Scanner inputLebar = new Scanner(System.in);

        System.out.print("Masukkan Panjang : ");
        int panjang = inputPanjang.nextInt();
        System.out.print("Masukkan Lebar : ");
        int lebar = inputLebar.nextInt();
        int luas = panjang * lebar;

        System.out.println("\n Processing... \n");
        System.out.println("Luas dari Persegi Panjang adalah " + luas);
        line();

        promptEnterKey();
        menuUtama();
    }


    //Volume Kubus 2.1
    public static void volumeKubus(){
        line();
        System.out.println("Anda memilih Kubus");
        line();

        Scanner inputSisi = new Scanner(System.in);

        System.out.print("Masukkan Sisi : ");
        int sisi = inputSisi.nextInt();
        int volume = (int) Math.pow(sisi, 3);

        System.out.println("\n Processing... \n");
        System.out.println("Volume dari Kubus adalah " + volume);
        line();

        promptEnterKey();
        menuUtama();
    }

    //Volume Balok 2.2
    public static void volumeBalok(){
        line();
        System.out.println("Anda memilih Balok");
        line();

        Scanner inputPanjang = new Scanner(System.in);
        Scanner inputLebar = new Scanner(System.in);
        Scanner inputTinggi = new Scanner(System.in);

        System.out.print("Masukkan Nilai Panjang : ");
        int panjang = inputPanjang.nextInt();
        System.out.print("Masukkan Nilai Lebar : ");
        int lebar = inputLebar.nextInt();
        System.out.print("Masukkan Nilai Tinggi : ");
        int tinggi = inputTinggi.nextInt();
        int volume = panjang * lebar * tinggi;

        System.out.println("\n Processing... \n");
        System.out.println("Volume dari Balok adalah " + volume);
        line();

        promptEnterKey();
        menuUtama();
    }


    //Volume Tabung 2.3
    public static void volumeTabung(){
        line();
        System.out.println("Anda memilih Tabung");
        line();

        Scanner inputTinggi = new Scanner(System.in);
        Scanner inputJariJari = new Scanner(System.in);

        System.out.print("Masukkan Nilai Tinggi : ");
        int tinggi = inputTinggi.nextInt();
        System.out.print("Masukkan Nilai Jari-Jari : ");
        int jariJari = inputJariJari.nextInt();
        double volume = 3.14 * tinggi * jariJari * jariJari;

        System.out.println("\n Processing... \n");
        System.out.println("Volume dari Tabung adalah " + volume);
        line();

        promptEnterKey();
        menuUtama();
    }
}
